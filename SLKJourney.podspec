Pod::Spec.new do |s|
  s.name                = 'shared'
  s.version             = '0.0.1'
  s.license             =  { :type => 'BSD' }
  s.homepage            = 'http://timesinternet.in'
  s.authors             = { 'Sanjay' => 'Sanjay.rathor@timesinternet.in' }
  s.summary             = 'A cocoa pod containing the shared framework.'
  s.source              = { :http => 'https://bitbucket.org/SanjaySinghRathor/sharedframework/downloads/shared.zip' }
  s.ios.deployment_target = '11.0'
#  s.source_files        = 'Classes/*.{h,m}'
  s.vendored_frameworks = 'shared.framework'
end

